const btnOpenModal = document.getElementById('openModal');
const modal = document.querySelector('.modal')
const close = document.querySelector('.modal-form__close');
const ua = navigator.userAgent,
  event = (ua.match(/iPadconst/i) || ua.match(/iPhone/)) ? "touchstart" : "click";

if (btnOpenModal) {
  btnOpenModal.addEventListener('click', () => {
    modal.classList.add('is-open');
    document.body.classList.add('is-locked-modal');
  });
}

if (close) {
  close.addEventListener('click', ()  => {
    modal.classList.remove('is-open');
    document.body.classList.remove('is-locked-modal');
  });
}

window.addEventListener(event, event => {
  if (event.target == modal) {
    modal.classList.remove('is-open');
    document.body.classList.remove('is-locked-modal');
  }
});