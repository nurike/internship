import Swiper from 'swiper/bundle';

const tabs = () => {
  const tab = document.querySelectorAll('.version__link');
  const content = document.querySelectorAll('.version__images');
  const bg = document.querySelectorAll('.version__bg');
  let tabNum;

  tab.forEach(item => {
    item.addEventListener('click', () => {
      tab.forEach(item => {
        item.classList.remove('is-active');
      });
      event.target.classList.add('is-active');
      tabNum = event.target.getAttribute('data-tab-num');
      showContent(tabNum);
    });
  });

  const showContent = () => {
    content.forEach(item => {
      item.classList.contains(tabNum) ? item.classList.add('is-active') : item.classList.remove('is-active');
    });
    bg.forEach(item => {
      item.classList.contains(tabNum) ? item.classList.add('is-active') : item.classList.remove('is-active');
    });
  }
};

tabs();

let swiperTab = new Swiper('.version__swiper-container', {
  observer: true,
  observeParents: true,
  slidesPerView: 'auto',
  spaceBetween: 30,
  loop: true,
  navigation: {
    nextEl: '.swiper-button-next',
    prevEl: '.swiper-button-prev',
  },
});
