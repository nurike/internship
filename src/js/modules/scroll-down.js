const scrollDown = document.querySelector('.hero__link--down');
const preview = document.querySelector('.preview');

if (scrollDown){
  scrollDown.addEventListener('click', (event) => {
    event.preventDefault();
    preview.scrollIntoView({
      block: "start",
      behavior: "smooth"
    })
  })
};