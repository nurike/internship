import Swiper from 'swiper/bundle';

const stepSlide = document.querySelectorAll('.steps-slider__slide');
const slideCurrent = document.querySelector('.steps-slider__counter');
let stepTabs = [];

stepSlide.forEach(item => {
  stepTabs.push(item.dataset.title);
})

let swiperStep = new Swiper('.steps-slider__container', {
  slidesPerView: 'auto',
  spaceBetween: 30,
  pagination: {
    el: '.steps-tab-pagination, .steps-slider-pagination__bullet',
    clickable: true,
    renderBullet: function (index, className) {
      return '<div class="' + className + '">\
      <span class="steps-tab__num">' + (index < 10 ? '0' + (index + 1) : (index + 1)) + '</span>\
      <span class="steps-tab__title">' + (stepTabs[index]) + '</span>\
      </div>';
    },
  },
  navigation: {
    nextEl: '.steps-slider-pagination__next',
    prevEl: '.steps-slider-pagination__prev',
  },
});

swiperStep.on('slideChange', () => {
  changeStep();
});

const changeStep = () => {
  const current = swiperStep.realIndex + 1;
  slideCurrent.innerHTML = current + '/' + swiperStep.slides.length;
}

changeStep();