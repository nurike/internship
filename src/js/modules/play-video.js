const videoPlay = document.querySelector('.online-control__video-play');
const video = document.querySelector('.online-control__video');
const videoCnt = document.querySelector('.online-control__video-cnt');
// const tag = document.createElement('script');
// tag.src = "https://www.youtube.com/player_api";
// const firstScriptTag = document.getElementsByTagName('script')[0];
// firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);
// let player;

// videoPlay.addEventListener('click', () => {
//   event.preventDefault();
//   player = new YT.Player('myPlayer', {
//     height: '100%',
//     width: '100%',
//     videoId: 'G1IbRujko-A',
//     events: {
//       'onReady': onPlayerReady,
//     }
//   });
// })

// const onPlayerReady = (event) => {
//   event.target.playVideo();
// }

videoPlay.addEventListener('click', () => {
  event.preventDefault();
  videoPlay.classList.add('is-active');
  video.classList.add('is-active');
  videoCnt.setAttribute('controls', '');
  videoCnt.play();
});
