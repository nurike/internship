import Swiper from 'swiper/bundle';

const article = document.querySelectorAll('.preview-tab__slide');
let tab = [];

article.forEach(item => {
  tab.push(item.dataset.parent);
})

let swiper = new Swiper('.swiper-container--wrap', {
  cssMode: true,
  slidesPerView: 'auto',
  spaceBetween: 30,
  pagination: {
    el: '.swiper-pagination.preview-tab-pagination',
    clickable: true,
    renderBullet: function (index, className) {
      return '<span class="' + className + '">' + (tab[index]) + '</span>';
    },
  },
  navigation: {
    nextEl: '.preview-slider-pagination__next',
    prevEl: '.preview-slider-pagination__prev',
  },
  mousewheel: true,
  keyboard: true,
});

let swiper1 = new Swiper('.swiper-container--wrap', {
  cssMode: true,
  slidesPerView: 'auto',
  spaceBetween: 30,
  pagination: {
    el: '.swiper-pagination.preview-slider-pagination__bullet',
    clickable: true,
  },
  mousewheel: true,
  keyboard: true,
});

let swiperSublevel = new Swiper('.swiper-sublevel', {
  slidesPerView: 'auto',
  spaceBetween: 30,
  navigation: {
    nextEl: '.swiper-button-next',
    prevEl: '.swiper-button-prev',
  },
});

let swiperMobile = new Swiper('.swiper-container--mobile', {
  slidesPerView: 'auto',
  spaceBetween: 30,
});
