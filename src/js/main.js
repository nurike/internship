import "core-js/stable";
import "@babel/polyfill";
import smoothscroll from 'smoothscroll-polyfill';
import "./modules/menu.js";
import "./modules/modal-form.js";
import "./modules/swiper.js";
import "./modules/tab.js";
import "./modules/steps.js";
import "./modules/scroll-down.js";
import "./modules/play-video.js";

smoothscroll.polyfill();
